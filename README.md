# GitLab Smoke Tests

Use GitLab CI to quickly test if GitLab features are working as intended.
Currently supported:

* [Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/index.html)
* [Maven Repository](https://docs.gitlab.com/ee/user/packages/maven_repository/index.html)
* [NPM Registry](https://docs.gitlab.com/ee/user/packages/npm_registry/index.html)

Each feature's smoke test is a CI job that needs to be manually triggered by
default. To run all tests automatically, set a `RUN_ALL_TESTS` CI/CD environment
variable.
